var app = require('app');
var BrowserWindow = require('browser-window');
var fs = require('fs');
var mkdirp = require('mkdirp');
var Express = require('express');
var http = require('http');
var connect = require('connect');
var serveStatic = require('serve-static');
var path = require('path');
var ipc = require('ipc');

require('crash-reporter').start();

var mainWindow = null;

app.on('window-all-closed', function(){
    if (process.platform != 'darwin')
    {
      app.quit();
    }
  });

app.on('ready', function(){
  mainWindow = new BrowserWindow(
    {
      width: 800,
      height: 600,
      "web-preferences": {
         "web-security": false
        }
    });


  mainWindow.loadUrl('file://./../../../../../Users/adamstaszak1/Documents/Projects/Electron/desktop.html');
  //mainWindow.openDevTools();


  var webService = Express();

  webService.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  webService.get('/sprawa/:nazwa', function(req, res) {
    mkdirp('KatalogiSpraw/' + req.params.nazwa, function (err) {
      if (err) console.error(err)
        else console.log('pow!')
    });
    res.send({nazwa:req.params.nazwa});
  });

  webService.listen(3000);
  console.log('WebService on port 3000...');

  connect().use(serveStatic('../Electron/Web')).listen(8080);
  

  mainWindow.on('closed', function(){
    mainWindow = null;
  });
});
